const express = require("express");
const app = express();
const bodyParser = require('body-parser');
const mysql = require("mysql2");
var connection = require("./config/db.config.js")
const port = 3333;

// Import routes
const indexUser = require("./routes/index")

// Gửi yêu cầu phân tích kiểu nội dung application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))


// var db = mysql.createConnection({   
//   host: '127.0.0.1',     
//   user: 'root',     
//   password: 'gfnPKI55',     
//   database: 'shop',
//   port: 3306
// });  
// Gửi yêu cầu phân tích kiểu nội dung application/json
app.use(bodyParser.json())

// Route middlewares
app.use('/api/user',indexUser)

// Lắng nghe các requests
// connection.query('SELECT * FROM user', function (error, results, fields) {
//     if (error) throw error;
//     console.log('The solution is: ', results);
//   });
app.listen(port, function(){
    console.log("Server listening port",+port)
})