const express = require("express");
var connection = require("../config/db.config.js");
const router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require("jsonwebtoken");
const secret = "duonng00";
const saltRounds = 10;
const salt = bcrypt.genSaltSync(saltRounds);
router.get("/", function (req, res, next) {
  res.send("Hello World");
});
router.post("/register", function (req, res, next) {
  let username = req.body.username;
  let password = req.body.password;

  let sql0 = "SELECT * FROM shop.user where username = ?";
  connection.query(sql0,username,function(err,results,fields){
    if(results.length == 1){
        res.send("username đã tồn tại");
    }else{
        const hash = bcrypt.hashSync(password, salt);
        let sql = "INSERT INTO shop.user(username,password) VALUE(?,?)";
        connection.query(sql, [username, hash], (err, rows, fields) => {
          if (!err) res.send("Register successfully.");
          else console.log(err);
        });
    }
  })
});
router.post("/login", function(req, res, next){
    let username = req.body.username;
    let password = req.body.password;
    let sql0 = "SELECT * FROM shop.user WHERE username = ? LIMIT 0, 1";


    connection.query(sql0,username,function(err,results,fields){
        
        if(results.length == 1){
            pass = results[0].password;
            if(bcrypt.compareSync(password, pass)){
                let token = jwt.sign({username:username,password:password},secret,{
                  expiresIn: 10
                });
                let sql = "UPDATE shop.user SET token = ? WHERE username = ?";
                connection.query(sql,[token,username],(err, rows,fields)=>{
                  if (!err) res.send("Login successfully.");
                  else console.log(err);
                })
            }else{
                res.send("Password is incorrect!")
            }
        }else{
            res.send("Người dùng không tồn tại!");
        }
    })
})
module.exports = router;
